import { Provider } from 'react-redux';
import store from './common/redux/store';
import Home from './core/components/Home';
// test
function App() {
  return (
    <Provider store={store}>
      <Home />
    </Provider>
  );
}

export default App;
