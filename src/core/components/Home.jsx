import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchCharactersAction } from "../../common/redux/actions/characterAction";

function Home() {
    const dispatch = useDispatch();
    const { characters } = useSelector(state => state.characterService);

    console.log(characters)

    const fetchCharacters = useCallback(() => dispatch(fetchCharactersAction()), [dispatch]);
    return (
        <div>
            <button onClick={fetchCharacters}>Get character</button>
        </div>
    );
}

export default Home;