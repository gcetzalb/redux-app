import { combineReducers } from "redux";
import characterService from "./reducers/characterReducer/characterReducer";

const appService = combineReducers({
    characterService
});

export default function reducers(state, action) {
    return appService(state, action);
}