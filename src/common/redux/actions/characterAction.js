import axios from 'axios';
import { CHARACTER_SUCCESS } from '../reducers/characterReducer/types';

const url = 'https://rickandmortyapi.com/api/character';

const setCharacterType = characters => ({
    type: CHARACTER_SUCCESS,
    characters
});

export const fetchCharactersAction = () => async dispatch => {
    try {
        const response = await axios.get(url);
        dispatch(setCharacterType(response.data))
    } catch (e) {

    }
}