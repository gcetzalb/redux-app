import { CHARACTER_SUCCESS } from './types';

const initialState = {
    characters: []
};

export default function characterReducer(state = initialState, action) {
    switch (action.type) {
        case CHARACTER_SUCCESS: 
            return {
                ...state,
                characters: action.characters
            }
        default:
            return state;
    }
}